__Multiball__ - hit bookcase for GREED lights. lock balls 1 and 2 with thing saucer or short plunge to swamp, start multiball by hitting chair scoop.

__Modes__ - shoot center ramp to relight chair scoop, shoot chair scoop when lit. can stack multiple modes. finish all modes for tour the mansion bonus 50M.

Be careful titling since bonus is applied at the end of the ball and can be significant from modes.
