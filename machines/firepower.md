completing stands lights locks for multiball, progress is generally lost between balls on stands, but locks will remain.

safer strategy is to keep sending the ball up to the lanes and complete them for bonus and bonus multipliers.

if you can complete a set of targets, the spinner will light for 1K/spin and can be good points if juiced.
