<a href="https://pintips.net/games/64" class="pintips">view pintips</a>

Shoot right and left ramps repeatedly to enter 'Jam' multiball. Loops for 'Tour' multiball, standups and drops for 'Album'. Start multiball by shooting right ramp when respective insert is lit.

Center bell multiple times will light 2x and 3x playfield multiplier.