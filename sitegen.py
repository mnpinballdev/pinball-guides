from jinja2 import Environment, Template, FileSystemLoader
import json
import os
import markdown
import shutil
import sys
import collections

output_dir = sys.argv[1] if len(sys.argv) > 1 else 'public'
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

mypath = 'assets'
for f in os.listdir(mypath):
    fullname = os.path.join(mypath, f)
    if os.path.isfile(fullname):
        shutil.copy(fullname, output_dir)

with open('db/machines.json') as data_file:    
    data = json.load(data_file)

for machine, machine_data in data.items():
    filename = 'machines/{0}.md'.format(machine)
    if os.path.isfile(filename):
        with open(filename) as md_content:
            machine_data['content'] = markdown.markdown(md_content.read())

# order the json data
machines = collections.OrderedDict(sorted(data.items(), key=lambda x : x[0]))

env = Environment(loader=FileSystemLoader('templates'))
template = env.get_template('index.html.j2')

with open('{0}/{1}'.format(output_dir, 'index.html'), 'w') as output_file:
    output_file.write(template.render(machines=machines))
